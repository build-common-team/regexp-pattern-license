use Test2::V0;

use Path::Tiny;
use Test::Synopsis::Expectation;

my $files = path('lib')->visit(
	sub {
		my ( $path, $state ) = @_;
		$state->{$path}++ if $_->is_file and /\.pm\z/;
	},
	{ recurse => 1 }
);

synopsis_ok [ keys %$files ];

done_testing;
